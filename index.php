<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Catastrophe{
    public $forme;
    function __construct($forme){
        $this->forme = $forme;
    }
    function catastrophe($individu){
        if(array_search(substr($individu->genome,0,1),$hex) !== $this->forme){
            $individu->Tuer();
        }
    }
}
class Monstre{
    function select($individu){
        if($individu->vivant !== true){
            if($individu->Viable() === false){
                echo "l'individu ".$individu->genome." s'est transformer";
            }else{
                echo "l'individu ".$individu->genome." est mort mais ne peux pas se transformer";
            }
            
        }else{
            echo "l'individu ".$individu->genome." ne peux pas se transformer car il est vivant";
        }
    }
}
class RayonGamma{
    public $intensite;
    public $couleur;  
    function __construct(){
        $this->intensite = random_int(0,15);
        $this->couleur = random_int(0,3);

    }
    function RayonGama($individu){
        $hex = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
        switch($this->couleur){
            case 1 : 
                // rouge
                if(array_search(substr($individu->genome,1,1),$hex) >= $this->intensite){
                    $individu->Tuer();
                    echo "l'individu ".$individu->genome." est mort du rayon gamma d'intensite ".$this->intensite." sur la couleur rouge";
                }
            break;
            case 2 : 
                // vert
                if(array_search(substr($individu->genome,2,-1),$hex) >= $this->intensite){
                    $individu->Tuer();
                    echo "l'individu ".$individu->genome." est mort du rayon gamma d'intensite ".$this->intensite." sur la couleur vert";
                }
            break;
            case 3 : 
                // bleu
                if(array_search(substr($individu->genome,-1),$hex) >= $this->intensite){
                    $individu->Tuer();
                    echo "l'individu ".$individu->genome." est mort du rayon gamma d'intensite ".$this->intensite." sur la couleur bleu";
                }
            break;
        }
    }
} 
class Selecteur{
    function select($individu){
        $malheur = random_int(1,3);
        $malheur = 3;
        switch($malheur){
            case 1 : 
                $cause = new RayonGamma();
                $cause->RayonGama($individu);
            break;
            case 2 : 
                $cause = new Catastrophe();
                $cause->catastrophe();
                
            break;
            case 3 : 
                $cause = new Monstre();
                $cause->select($individu);
            break;
        }
    }
}
class Population
{
    public $population = [];

    function mutuation($facteur){
        $hex = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
        foreach($this->population as $individu){
            $mutation = [
                array_search(substr($individu->genome,0,1),$hex),
                array_search(substr($individu->genome,1,1),$hex),
                array_search(substr($individu->genome,2,-1),$hex),
                array_search(substr($individu->genome,-1),$hex)
            ];
            $new_genome = "";
            foreach($mutation as $gen){
                if(random_int(0,100) <= $facteur){
                    if(random_int(0,1) === 1){
                        if(isset($hex[$gen + 1])){
                            $new_genome .= $hex[$gen + 1];
                        }else{
                            $individu->Tuer();
                        }
                    }else{
                        if(isset($hex[$gen - 1])){
                            $new_genome .= $hex[$gen - 1];
                        }else{
                            $individu->Tuer();
                        }
                    }
                }else{
                    $new_genome .= $hex[$gen];
                    echo "L'individu :".$individu->genome." a esquiver la mutuation d'un gene ";
                    echo '<br></br>';
                } 
                
            }   
                $individu->genome = $new_genome ;
                $individu->viable();
                var_dump($individu->genome,$individu->vivant);
                echo '<br></br>';
        }
    }
    function selection(){
        
    }
    function add($individu){
        array_push($this->population,$individu);
    }

    function getVivants(){
        foreach($this->population as $individu){
            if($individu->vivant !== true){
                unset($this->population[array_search($individu,$this->population)]);
            }
        }
        return $this->population;
    }

    function reproduction(){
        $new_population = new Population();
        for($i = 0;$i <= count($this->population);$i += 2){
            if(isset($this->population[$i]) && isset($this->population[$i+1])){
                $reproduction = ['h1' => $this->population[$i],'h2' => $this->population[$i+1]];
                    $rand = random_int(1,2);
                    if($rand === 1){
                        $nvx_genome = substr($reproduction['h'.$rand]->genome,0,2).substr($reproduction['h'.$rand+1]->genome,2,4);
                    }else{
                        $nvx_genome = substr($reproduction['h'.$rand]->genome,0,2).substr($reproduction['h'.$rand-1]->genome,2,4);
                    }
                    $new_population->add(new Individu($nvx_genome));

            }else if(isset($this->population[$i])){
                echo "l'humain :".$this->population[$i]->genome." se sent tres seul";
            }
        }
        return $new_population;
    }
}
class Individu extends Population
{
    public $genome;
    public $vivant = true; 
    function __construct($genome){
       $this->genome = $genome;
    }

    function Viable(){
        $viable = true;
        $hex = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
        if(substr($this->genome,0,1) > 2 || substr($this->genome,0,1) < 0){
            $viable = false;
        }
        if(!in_array(substr($this->genome,1,1),$hex)){
            $viable = false;
        }
        if(!in_array(substr($this->genome,2,-1),$hex)){
            $viable = false;
        }
        if(!in_array(substr($this->genome,-1),$hex)){
            $viable = false;
        }
        if($viable !== true){
            $this->Tuer();
            return false;
        }else{
            echo "l'individu ".$this->genome." est viable. ";
        }
    }

    function Tuer(){
        $this->vivant = false;
    }

    function GetGenome(){
        return $this->genome;
    }
}

class IndivuFactory{
    function getIndivu(){
        $hex = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
        $genome = random_int(0,2).$hex[random_int(0,15)].$hex[random_int(0,15)].$hex[random_int(0,15)];
        return new Individu($genome);
    }

    function getPopulation($n){
        $population = new Population();
        for($i = 0;$i<=$n;$i++){
            $population->add($this->getIndivu());
        }
        return $population;
    }
}

$test = new IndivuFactory();
// $rouge = $test->getPopulation(2);
// foreach($rouge as $select){var_dump($select);echo '<br></br>';}
// //$new_pop = $rouge->reproduction();
// //foreach($new_pop as $select){var_dump($select);echo '<br></br>';}
// $rouge->mutuation(90);
// var_dump($rouge->getVivants());
$testIndividu = $test->getIndivu();
$testIndividu->Tuer();
var_dump($testIndividu);
$select = new Selecteur();
$monstre = new Individu('23FD');
$monstre->Tuer();
$select->select($monstre);


